\select@language {croatian}
\contentsline {chapter}{\numberline {1}Uvod}{1}
\contentsline {chapter}{\numberline {2}Duboko u\IeC {\v c}enje i tehnologije}{3}
\contentsline {section}{\numberline {2.1}Osnove dubokog u\IeC {\v c}enja}{3}
\contentsline {section}{\numberline {2.2}Tensorflow}{4}
\contentsline {section}{\numberline {2.3}Keras}{4}
\contentsline {chapter}{\numberline {3}Neuronske mre\IeC {\v z}e i slojevi}{5}
\contentsline {chapter}{\numberline {4}Web aplikacija}{6}
\contentsline {section}{\numberline {4.1}Django}{6}
\contentsline {section}{\numberline {4.2}Programski kod i obje\IeC {\v s}njenje stranice}{6}
\contentsline {chapter}{\numberline {5}Zaklju\IeC {\v c}ak}{7}
\contentsline {chapter}{Literatura}{8}
