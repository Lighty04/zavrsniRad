from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import UploadFileForm

def index(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
    else:
        form = UploadFileForm()
    return render(request, 'template.html', {'form': form})
