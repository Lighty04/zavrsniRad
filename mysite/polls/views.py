from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect
from django.core.files import File
import base64
from io import BytesIO

from django.shortcuts import render
from keras.models import load_model
import keras
import PIL
from io import BytesIO
from PIL import Image
import numpy as np
import sys, cv2


@csrf_exempt
def index(request):

  if("fileToUpload" in request.FILES):
    with open('slika_fer_nesto.png', 'wb+') as destination:
        for chunk in request.FILES["fileToUpload"].chunks():
            destination.write(chunk)
    image1 = Image.open("slika_fer_nesto.png")
    buffer2 = BytesIO()
    image1.save(buffer2, format="PNG")
    string1 = base64.b64encode(buffer2.getvalue())
    string1 = b'data:image/png;base64,'+string1

  else:
    string1 = request.POST.get("hidden_name")
    string = string1[len("data:image/png;base64,"):]
    handle_uploaded_file(string)

  lista = detectEmotion("slika_fer_nesto.png")
  keras.backend.tensorflow_backend.clear_session()



  rezz = []
  for slika in lista:
    buffer1 = BytesIO()
    slika[0][0].save(buffer1, format="PNG")
    slika_base64 = base64.b64encode(buffer1.getvalue())

    i=0
    ocjena=0.0
    max1=0
    for ocjenaEmocija in slika[1][0]:
      if ocjenaEmocija > ocjena:
        ocjena = ocjenaEmocija
        max1=i
      i+=1

    emocije = ["Nemoj da se ljutiš (Ljutnja)", "Kaj ti se gadi? (Gađenje)", "Čeg se bojiš šefe? (Bojaznost)", "Kad si sretan lupi dlanom ti od dlan (Sreća)", "Baš ti lepo stoje suze al nemoj plakati (Tuga)", "Malo smo se iznandili je li (Iznenađenost)", "Neutralan si mi nešto (Neutralnost)"]
    rezEmo=emocije[max1]
      
    rezz.append((b'data:image/png;base64,'+slika_base64, rezEmo))
  return render(request, "ispisiRezultat.html", { "slika" : string1,  "rezultati" : rezz})

def handle_uploaded_file(f):
  with open('slika_fer_nesto.png', 'wb+') as destination:
    destination.write(base64.b64decode(f))

def decode_base64(data):
  missing_padding = len(data) % 4
  if missing_padding != 0:
    data += b'='* (4 - missing_padding)
  return base64.decodestring(data)




def cascade_detect(cascade, image):
  gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
  return cascade.detectMultiScale(
    gray_image,
    scaleFactor = 1.15,
    minNeighbors = 5,
    minSize = (30, 30),
  )

def detections_draw(image, detections):
  for (x, y, w, h) in detections:
    print("({0}, {1}, {2}, {3})".format(x, y, w, h))
    #cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)

def detect_faces(cascade_path,image_path):
  cascade = cv2.CascadeClassifier(cascade_path)
  image = cv2.imread(image_path)
  detections = cascade_detect(cascade, image)
  detections_draw(image, detections)
  print("Found {0} objects!".format(len(detections)))
  cv2_im = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
  pil_im = Image.fromarray(cv2_im)
  listaSlika = []
  for (x, y, w, h) in detections:
    listaSlika.append((pil_im.crop((x+5,y+5,x+w-5,y+h-5)),(x-5,y-5,x+w-5,y+h-5)))
  return listaSlika

def detectEmotion(image_name):
  listaSlika = detect_faces("polls/cascade.xml", image_name)
  emocije = ["Angry", "Disgust", "Fear", "Happy", "Sad", "Surprise", "Neutral"]
  model = load_model("polls/save_weights.h5")
  print(emocije)
  listaVrati = []
  for img in listaSlika:
    im = img[0].resize((48, 48), PIL.Image.ANTIALIAS).convert('L')
    pixels = list(im.getdata())
    listaNDarraya = []
    l = list(map(lambda x : int(x), pixels))
    listaNDarraya.append(l)
    n = np.ndarray((1,48,48,1), buffer=np.array(listaNDarraya), dtype=int)
    score = model.predict(n,verbose=0)
    listaVrati.append((img,score))
  return listaVrati
