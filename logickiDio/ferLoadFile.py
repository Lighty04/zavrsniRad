import numpy as np

#CSV file has only emotion and image values in this order to be parsed well
#FER 2013 has 28709 examples with pictures size of 48x48

def readTrainingFileFER2013CSV():
    fileFerStream = open("trainingDataFer.csv", "r")
    l = None
    listaNDarraya = []
    listaOfY = []
    for line in fileFerStream:
        elementsOfFile = line.split(",")
        listaOfY.append(int(elementsOfFile[0]))
        l = elementsOfFile[1].split(" ")
        l = list(map(lambda x : int(x), l))
        listaNDarraya.append(l)
    n = np.ndarray((28709+7178,48,48), buffer=np.array(listaNDarraya), dtype=int)
    m = np.ndarray((28709+7178,), buffer=np.array(listaOfY), dtype=int)
    return (n, m)

def readTestingFileFER2013CSV():
    fileFerStream = open("testingDataFer.csv", "r")
    l = None
    listaNDarraya = []
    listaOfY = []
    for line in fileFerStream:
        elementsOfFile = line.split(",")
        listaOfY.append(int(elementsOfFile[0]))
        l = elementsOfFile[1].split(" ")
        l = list(map(lambda x : int(x), l))
        listaNDarraya.append(l)
    n = np.ndarray((7178,48,48), buffer=np.array(listaNDarraya), dtype=int)
    m = np.ndarray((7178,), buffer=np.array(listaOfY), dtype=int)
    return (n, m)

