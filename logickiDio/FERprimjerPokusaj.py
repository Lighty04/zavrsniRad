from __future__ import print_function
import keras
from pathlib import Path
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, Convolution2D, MaxPooling2D, AveragePooling2D
from keras import backend as K
from keras.models import load_model
from keras.optimizers import Adadelta
import numpy as np
import os
import ferLoadFile as flf
import modelGenerate as gm

batch_size = 128
num_classes = 7
epochs = 1

fileO = open("brojiEpohe.txt", 'a+')
fileO.write("\n"+str(epochs))
fileO.close()

# input image dimensions
img_rows, img_cols = 48, 48

# the data, shuffled and split between train and test sets
(x_train, y_train) = flf.readTrainingFileFER2013CSV()
(x_test, y_test) = flf.readTestingFileFER2013CSV()


if K.image_data_format() == 'channels_first':
	x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
	x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
	input_shape = (1, img_rows, img_cols)
else:
	x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
	x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
	input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

fileName = "save_weights.h5"
model = None
if Path(fileName).is_file():
	print("Učitavanje modela imena => " + fileName)
	model = load_model(fileName)

	score = model.evaluate(x_test, y_test, verbose=0)
	print('Zadnji rezultati su bili:')
	print('Test loss:', score[0])
	print('Test accuracy:', score[1])
else:
	print("Ne postoji spremljen model, radi se novi !!!")
	model = gm.model_generate()

	ada = Adadelta(lr=0.1, rho=0.95, epsilon=1e-08)
	model.compile(loss='categorical_crossentropy', optimizer=ada, metrics=['accuracy'])

model.summary()

model.fit(x_train, y_train,
	  batch_size=batch_size,
	  epochs=epochs,
	  verbose=1,
	  validation_data=(x_test, y_test))

score = model.evaluate(x_test, y_test, verbose=0)

model.save("save_weights.h5")

print('Test loss:', score[0])
print('Test accuracy:', score[1])
