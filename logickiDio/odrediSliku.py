from keras.models import load_model
import PIL
from io import BytesIO
from PIL import Image
import numpy as np
import sys, cv2

def cascade_detect(cascade, image):
  gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
  return cascade.detectMultiScale(
    gray_image,
    scaleFactor = 1.15,
    minNeighbors = 5,
    minSize = (30, 30),
  )

def detections_draw(image, detections):
  for (x, y, w, h) in detections:
    print("({0}, {1}, {2}, {3})".format(x, y, w, h))
    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)

def detect_faces(cascade_path,image_path):
  cascade = cv2.CascadeClassifier(cascade_path)
  image = cv2.imread(image_path)
  detections = cascade_detect(cascade, image)
  detections_draw(image, detections)
  print("Found {0} objects!".format(len(detections)))
  cv2_im = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
  pil_im = Image.fromarray(cv2_im)
  listaSlika = []
  for (x, y, w, h) in detections:
    listaSlika.append((pil_im.crop((x+5,y+5,x+w-5,y+h-5)),(x-5,y-5,x+w-5,y+h-5)))
  return listaSlika

def detectEmotion(image_name):
  listaSlika = detect_faces("cascade.xml", image_name)
  emocije = ["Angry", "Disgust", "Fear", "Happy", "Sad", "Surprise", "Neutral"]
  model = load_model("save_weights.h5")
  print(emocije)
  for img in listaSlika:
    im = img[0].resize((48, 48), PIL.Image.ANTIALIAS).convert('L')
    pixels = list(im.getdata())
    listaNDarraya = []
    l = list(map(lambda x : int(x), pixels))
    listaNDarraya.append(l)
    n = np.ndarray((1,48,48,1), buffer=np.array(listaNDarraya), dtype=int)
    score = model.predict(n,verbose=0)
    print(score[0])
    im.show()

detectEmotion("slike/emocije.jpg")
