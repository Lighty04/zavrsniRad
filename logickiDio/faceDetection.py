import sys, cv2
from PIL import Image

def cascade_detect(cascade, image):
  gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
  return cascade.detectMultiScale(
    gray_image,
    scaleFactor = 1.15,
    minNeighbors = 5,
    minSize = (30, 30),
  )

def detections_draw(image, detections):
  for (x, y, w, h) in detections:
    print("({0}, {1}, {2}, {3})".format(x, y, w, h))
    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)

def detect_faces(cascade_path,image_path):
  cascade = cv2.CascadeClassifier(cascade_path)
  image = cv2.imread(image_path)
  detections = cascade_detect(cascade, image)
  detections_draw(image, detections)
  print("Found {0} objects!".format(len(detections)))
  cv2_im = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
  pil_im = Image.fromarray(cv2_im)
  listaSlika = []
  for (x, y, w, h) in detections:
    listaSlika.append((pil_im.crop((x+5,y+5,x+w-5,y+h-5)),(x-5,y-5,x+w-5,y+h-5)))
  return listaSlika
